import {createStore} from 'react-redux-app-container'
import filesReducer from 'ducks/files'
import rootsReducer from 'ducks/roots'

export default () => {
    const initialState = {
        files: {
            files: [
                {
                    path: 'local://test/',
                    mimetype: 'inode/directory',
                },
                {
                    path: 'local://3.jpg',
                    mimetype: 'image/jpeg',
                }, {
                    path: 'local://4.jpg',
                    mimetype: 'image/jpeg',
                },
            ],
            selected: null,
            directory: '/',
        },
        roots: {
            roots: [
                'local://',
                'amazon://',
            ],
            current: 'local://',
        }
    }
    const reducers = {
        files: filesReducer,
        roots: rootsReducer,
    }
    const middlewares = []
    const enhancers = []

    return createStore(initialState, reducers, middlewares, enhancers)
}

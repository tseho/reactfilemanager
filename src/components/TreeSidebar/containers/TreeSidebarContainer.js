import { connect } from 'react-redux'
import { selectRoot } from 'ducks/roots'
import TreeSidebar from '../components/TreeSidebar'

const mapDispatchToProps = {
    selectRoot,
}

const mapStateToProps = (state) => ({
    roots: state.roots.roots,
})

export default connect(mapStateToProps, mapDispatchToProps)(TreeSidebar)

import React, { Component } from 'react'
import TreeSidebar from 'components/TreeSidebar'
import FilesList from 'components/FilesList'

class App extends Component {
    render() {
        return (
            <div>
                <h2>Files</h2>
                <TreeSidebar />
                <FilesList />
            </div>
        )
    }
}

export default App
